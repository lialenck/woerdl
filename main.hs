{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.Trans.Except
import Control.Monad.Trans
import Control.Monad (guard)
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as B8
import Data.List (foldl', sortOn, elemIndex)
import Data.Bits (Bits, shiftR, shiftL, (.&.), (.|.))
import Data.Word (Word8, Word64)
import qualified Data.IntMap.Strict as I

pos_mod :: Word8 -> BS.ByteString -> Int -> Word64
pos_mod a bs i =
    if bs `BS.index` i == a
    then 3
    else if a `BS.elem` bs
        then 1
        else 0

interfere :: BS.ByteString -> BS.ByteString -> Word64
interfere as bs = fst $! BS.foldl' (\o n -> shift o n) (0, 0) as
    where shift (o, i) n = ((o `shiftL` 2) .|. pos_mod n bs i, i + 1)

showInterference :: (Show i, Num i, Bits i) => i -> String
showInterference x =
        (toChar $ x `shiftR` 8) ++
        (toChar $ (x `shiftR` 6) .&. 3) ++
        (toChar $ (x `shiftR` 4) .&. 3) ++
        (toChar $ (x `shiftR` 2) .&. 3) ++
        (toChar $ x .&. 3)
    where toChar x = case x of
            0 -> " "
            1 -> "I"
            3 -> "E"
            _ -> error $ "invalid num: " ++ show x

readInterference s =
        (fromChar (s !! 0) `shiftL` 8) .|.
        (fromChar (s !! 1) `shiftL` 6) .|.
        (fromChar (s !! 2) `shiftL` 4) .|.
        (fromChar (s !! 3) `shiftL` 2) .|.
        fromChar (s !! 4)
    where fromChar c = case c of
            ' ' -> 0
            'I' -> 1
            'E' -> 3
            _ -> error $ "invalid char: " ++ show c
        

without _ [] = []
without 0 (a:as) = as
without n (a:as) = a : without (n - 1) as

buildCounts :: BS.ByteString -> [BS.ByteString] -> I.IntMap Int
buildCounts w ws = foldl' (\o n -> I.insertWith (+) (fromIntegral $ interfere w n) 1 o) I.empty ws

-- how much information did these interference patterns give us?
-- |The first int should be the total count of words, which should be known.
countsInformation :: Int -> I.IntMap Int -> Double
countsInformation i m = I.foldl' (\o e -> o + (fromIntegral e / fromIntegral i)^2) 0.0 m

rateWord w ws = countsInformation (length ws) $ buildCounts w ws

filterPat pat prev_word ws =
    let pat_num = readInterference pat
    in filter (\w -> interfere prev_word w == pat_num) ws

exceptGuard s pred = if pred then except $ Right () else except $ Left s

interactiveFilter :: [BS.ByteString] -> [BS.ByteString] -> ExceptT String IO ()
interactiveFilter possible_ws all_ws = do
    liftIO $ putStrLn "What word did you input?"
    nextWord <- B8.pack <$> liftIO getLine -- TODO check for 5 chars
    exceptGuard "Invalid word length (must be 5)" $ BS.length nextWord == 5

    liftIO $ putStrLn "What pattern did you get? (Example: \"I EI \")"
    pat <- liftIO getLine -- TODO check for validity
    exceptGuard "Invalid pattern" (length pat == 5 && all (`elem` [' ', 'I', 'E']) pat)

    let possible_ws' = filterPat pat nextWord possible_ws

    case length possible_ws' of
        0 -> liftIO $ putStrLn "No more words left :("
        1 -> liftIO $ putStrLn $ "Answer: " ++ show (head possible_ws')
        n -> do
            liftIO $ putStrLn $ "There are " ++ show n ++ " possible words left, e.g.:"
            liftIO $ mapM_ print $ take 25 possible_ws'

            liftIO $ putStrLn "\nThe next few top picks are (lower scores are better):"
            let wordRatings = map (\w -> (w, rateWord w possible_ws')) all_ws
            let top_10 = take 10 $ sortOn snd wordRatings
            liftIO $ flip mapM_ top_10 $ \(w, r) -> putStrLn $ show w ++ ": " ++ show r

            interactiveFilter possible_ws' all_ws

main = do
    let nl = fromIntegral $ fromEnum '\n'
    ws <- filter ((==5) . BS.length) . fmap BL.toStrict . BL.split nl <$> BL.readFile "5-chars.txt"

    let sample = take 500 ws
    let sampleRatings = map (\w -> (w, rateWord w sample)) ws
    let probablyTop = map fst $ take 500 $ sortOn snd sampleRatings

    let wordRatings = map (\w -> (w, rateWord w ws)) probablyTop
    let top_50 = take 50 $ sortOn snd wordRatings

    flip mapM_ top_50 $ \(w, r) -> putStrLn $ show w ++ ": " ++ show r

    e <- runExceptT $ interactiveFilter ws ws
    case e of
        Left err -> putStrLn $ "Error: " ++ err
        Right _ -> return ()
